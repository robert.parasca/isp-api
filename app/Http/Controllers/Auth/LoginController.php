<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $request->validate([
            'user' => 'required|string',
            'password' => 'required|string'
        ]);

        $user = User::where('name', request('user'))->first();

        if ($user && Hash::check($request->input('password'), $user->password)) {

            $tokenObj = $user->createToken('Personal Access Token');
            return response()->json([
                'access_token' => $tokenObj->accessToken,
                'type' => 'Bearer',
                'expiries_in' => Carbon::parse($tokenObj->token->expires_at)->toDateTimeString()
            ], 200);
        }

        return response()->json([
            'message' => 'Invalid credentials'
        ], 401);
    }
}
